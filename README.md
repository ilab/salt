ilab-salt
=========

This is the salt `state` repository for `ilab` machine `vaduz`.

    vaduz.imp.fu-berlin.de


Usage
-----

To deploy new changes, one should push this repository to master,
connect to the machine and run:

    sudo salt-call state.apply

Doc: [salt-states](https://docs.saltstack.com/en/latest/topics/tutorials/walkthrough.html#salt-states)


Machine information
-------------------

* CI user is `jenkins`
* Build drive is mounted to `/srv/ilab-builds`
* It can be accessed through ssh using public key authentication
  * From the outside of FU, users should go through the ssh frontend
    `ProxyCommand ssh login.imp.fu-berlin.de -W %h:%p`
* Boards PORT is symlinked to `/dev/riot/tty-$(BOARD)`
  * Only one board of each type for the moment
  * The goal is not to rely on this but hels setting up quickly
* Notes on USB3 to USB2.
  * There are issues with usb3 and xhci as it does not allow enough devices
    I tried this manually, lets see if it survives a reboot
    `sudo setpci -H1 -d 8086:8c31 d0.l=0`
    https://www.systutorials.com/241533/how-to-force-a-usb-3-0-port-to-work-in-usb-2-0-mode-in-linux/
