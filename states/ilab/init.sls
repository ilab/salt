configure_make.packages:
  pkg.installed:
    - pkgs:
      - make
      - libtool
      - pkg-config
      - autoconf
      - automake
      - texinfo

############
# Flashers #
############

# Use openocd from 'gnu-mcu-eclipse' as they release newer versions
install_openocd:
  cmd.run:
    - creates: /opt/gnu-mcu-eclipse/openocd/0.10.0-12-20190422-2015
    - name: rm -rf /opt/gnu-mcu-eclipse && tar -C /opt -xf /var/cache/ilab_salt/openocd/gnu-mcu-eclipse-openocd-0.10.0-12-20190422-2015-centos64.tgz && ln -nfs /opt/gnu-mcu-eclipse/openocd/0.10.0-12-20190422-2015/bin/openocd /usr/local/bin/openocd
    - require:
      - file: /var/cache/ilab_salt/openocd/gnu-mcu-eclipse-openocd-0.10.0-12-20190422-2015-centos64.tgz
  file.managed:
    - name: /var/cache/ilab_salt/openocd/gnu-mcu-eclipse-openocd-0.10.0-12-20190422-2015-centos64.tgz
    - source: https://github.com/gnu-mcu-eclipse/openocd/releases/download/v0.10.0-12-20190422/gnu-mcu-eclipse-openocd-0.10.0-12-20190422-2015-centos64.tgz
    - source_hash: 9a601ded65cc5a8c3537f5bc8e14b2c71c01188d4cd2e5192833fcd6950e992f
    - makedirs: True
    - user: root
    - group: root
    - mode: '0644'

avrdude_pkg:
  pkg.installed:
    - name: avrdude

# Download and install Jlink.
# It should be queried with a 'POST' that accepts the ULA
install_jlink:
  pkg.installed:
    - sources:
      - jlink: /var/cache/ilab_salt/jlink/JLink_Linux_V650b_x86_64.deb
    - require:
      - cmd: /var/cache/ilab_salt/jlink/JLink_Linux_V650b_x86_64.deb
/var/cache/ilab_salt/jlink/JLink_Linux_V650b_x86_64.deb:
  cmd.run:
    - creates: /var/cache/ilab_salt/jlink/JLink_Linux_V650b_x86_64.deb
    - name: mkdir -p /var/cache/ilab_salt/jlink && wget --post-data 'accept_license_agreement=accepted&non_emb_ctr=confirmed&submit="Download software"' https://www.segger.com/downloads/jlink/JLink_Linux_V650b_x86_64.deb -O /var/cache/ilab_salt/jlink/JLink_Linux_V650b_x86_64.deb

install_uniflash:
  pkg.installed:
    - pkgs:
      - libnotify4
      - libcanberra0
      - libpython2.7
  cmd.run:
    - creates: /opt/ti/uniflash
    - name: rm -r /opt/ti/uniflash; /var/cache/ilab_salt/uniflash/uniflash_sl.4.6.0.2176.run --prefix /opt/ti/uniflash --mode unattended
    - onchanges:
      - file: /var/cache/ilab_salt/uniflash/uniflash_sl.4.6.0.2176.run
    - require:
      - file: /var/cache/ilab_salt/uniflash/uniflash_sl.4.6.0.2176.run
  file.managed:
    - name: /var/cache/ilab_salt/uniflash/uniflash_sl.4.6.0.2176.run
    - source: http://software-dl.ti.com/ccs/esd/uniflash/uniflash_sl.4.6.0.2176.run
    - source_hash: 819edc44616c3ecdbf5652aba3ebfa98d150ff71
    - makedirs: True
    - user: root
    - group: root
    - mode: '0755'


python3_pkg:
  pkg.installed:
    - pkgs:
      - python3
      - python3-dev
      - python3-pip
      - python3-virtualenv

python3_tests_pkg:
  pkg.installed:
    - pkgs:
      - python3-serial
      - python3-pexpect
      - python3-crypto
      - python3-pytest

python3_pip_pkg:
  file.managed:
    - name: /var/cache/ilab_salt/python3_pip_pkg/requirements.txt
    - source: salt://ilab/requirements.txt
    - makedirs: True
  cmd.run:
    - name: pip3 install --upgrade --requirement /var/cache/ilab_salt/python3_pip_pkg/requirements.txt
    - onchanges:
      - file: /var/cache/ilab_salt/python3_pip_pkg/requirements.txt

riot_term_pkg:
  pkg.installed:
    - pkgs:
      - picocom
      - python-serial  # miniterm.py
      - socat

tests_pkg:
  pkg.installed:
    - pkgs:
      - tcpdump
      - iproute2
# Put in users PATH
/usr/local/bin/tcpdump:
  file.symlink:
    - target: /usr/sbin/tcpdump
/usr/local/bin/bridge:
  file.symlink:
    - target: /sbin/bridge

# Should this be on the host?
python3_mcuboot_imgtool:
  pkg.installed:
    - pkgs:
      - python3-crypto
      - python3-ecdsa
      - python3-pyasn1

jenkins_sshkeys:
  ssh_auth.present:
  - name: jenkins
  - user: jenkins
  - source: salt://ilab/id_rsa_jenkins.pub

jenkins_slave_pkg:
  pkg.installed:
    - pkgs:
      - openjdk-8-jre

us_locale:
  locale.present:
    - name: en_US.UTF-8
default_locale:
  locale.system:
    - name: en_US.UTF-8
    - require:
      - locale: us_locale

# Groups management
# All users should be in `jenkins` group to use `/srv/ilab-builds` directory
jenkins_group:
  group.present:
    - name: jenkins
    - system: True
    - addusers:
      - jenkins
      - harter
      - jcarrano
      - mlenders
      - kernes92
# Access boards
dialout_group:
  group.present:
    - name: dialout
    - system: True
    - addusers:
      - jenkins
      - harter
      - jcarrano
      - mlenders
      - kernes92
# Access boards
plugdev_group:
  group.present:
    - name: plugdev
    - system: True
    - addusers:
      - jenkins
      - harter
      - jcarrano
      - mlenders
      - kernes92
# Boards udev rename rules
/etc/udev/rules.d/70-riot-boards.rules:
  file.managed:
    - source: salt://ilab/70-riot-boards.rules
    - user: root
    - group: root
    - mode: '0644'
  cmd.run:
    - name: udevadm control --reload
    - onchanges:
      - file: /etc/udev/rules.d/70-riot-boards.rules

# Boards ykush udev rules
/etc/udev/rules.d/60-ykush.rules:
  file.managed:
    - source: salt://ilab/60-ykush.rules
    - user: root
    - group: root
    - mode: '0644'
  cmd.run:
    - name: udevadm control --reload
    - onchanges:
      - file: /etc/udev/rules.d/60-ykush.rules

time_pkg:
  pkg.installed:
    - name: time

# /srv/ilab-builds
acl_pkg:
  pkg.installed:
    - name: acl

acl_ilab-builds:
  acl.present:
    - name: /srv/ilab-builds
    - acl_type: default:group
    - acl_name: jenkins
    - perms: rwx
    - recurse: True
    - require:
      - pkg: acl_pkg

# Workspace RC file
/srv/ilab-builds/workspace/workspace.rc:
  file.managed:
    - source: salt://ilab/workspace.rc
    - user: root
    - group: root
    - mode: '0644'

# Other users
/etc/sudoers.d/51_host_users:
  file.managed:
    - source: salt://ilab/51_host_users
    - user: root
    - group: root
    - mode: '0400'


# Configure USB2 ports to use ehci controller
setpci_usb2_ehci_systemd_unit:
  file.managed:
    - name: /etc/systemd/system/setpci-usb2-ehci.service
    - mode: '0644'
    - source: salt://ilab/setpci-usb2-ehci.service
  module.run:
    - name: service.systemctl_reload

setpci_usb2_ehci_running:
  service.running:
    - name: setpci-usb2-ehci
    - enable: True
    - watch:
      - module: setpci_usb2_ehci_systemd_unit

# This should match 'GIT_CACHE_DIR' from 'workspace.rc'
/srv/ilab-builds/cache/gitcache:
  cmd.run:
    - name: git init --bare /srv/ilab-builds/cache/gitcache
    - creates: /srv/ilab-builds/cache/gitcache
    - runas: jenkins
