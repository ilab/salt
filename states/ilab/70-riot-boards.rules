# Name all boards PORT as /dev/riot/ttyBOARD_NAME
#
#     udevadm info  /dev/ttyACM0
#     ID_VENDOR_ID == ATTRS{idVendor}
#     ID_MODEL_ID  == ATTRS{idProduct}
#
#     udevadm info --attribute-walk --name /dev/ttyACM0
#     # for more detailed output with `ATTRS`

# For the Silicon Labs USB UART converter, the serial number can be changed
# with CP21xxCustomizationUtility.exe from USBXpress_SDK.exe (in a win VM)

# arduino-mega2560
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="2a03", ATTRS{idProduct}=="0042", SYMLINK+="riot/tty-arduino-mega2560"
# ek-lm4f120xl
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="1cbe", ATTRS{idProduct}=="00fd", ATTRS{serial}=="0E214FFE", SYMLINK+="riot/tty-ek-lm4f120xl"
# esp32-wroom-32
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="CP2102N USB to UART Bridge Controller", ATTRS{serial}=="d27cb210df3de81181be72c97a7060d0", SYMLINK+="riot/tty-esp32-wroom-32"
# esp8266-esp-12x (nodeMCU Amica)
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="CP2102 USB to UART Bridge Controller", ATTRS{serial}=="8266", SYMLINK+="riot/tty-esp8266-esp-12x"
# frdm-k64f
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0d28", ATTRS{idProduct}=="0204", ATTRS{manufacturer}=="MBED", ATTRS{serial}=="02400221C3093E0E3ED7C3B6", SYMLINK+="riot/tty-frdm-k64f"
# frdm-kw41z
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="1366", ATTRS{idProduct}=="1015", ATTRS{manufacturer}=="SEGGER", ATTRS{serial}=="000621000000", SYMLINK+="riot/tty-frdm-kw41z"
# msba2
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{manufacturer}=="FU Berlin", ATTRS{product}=="MSB430A", ATTRS{serial}=="ARM", SYMLINK+="riot/tty-msba2"
# mulle
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", ATTRS{manufacturer}=="Eistec AB", ATTRS{serial}=="337", SYMLINK+="riot/tty-mulle"
# nrf52dk
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="1366", ATTRS{idProduct}=="1015", ATTRS{manufacturer}=="SEGGER", ATTRS{serial}=="000682497679", SYMLINK+="riot/tty-nrf52dk"
# nucleo-f103rb
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", ATTRS{manufacturer}=="STMicroelectronics", ATTRS{serial}=="066FFF525750877267064625", SYMLINK+="riot/tty-nucleo-f103rb"
# pba-d-01-kw2x
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0d28", ATTRS{idProduct}=="0204", ATTRS{manufacturer}=="MBED", ATTRS{serial}=="02000203C37B4E073E87B3FF", SYMLINK+="riot/tty-pba-d-01-kw2x"
# pic32-wifire
# The 'pickit3' receives a '/dev/hidraw.?' but I did not map it to a symlink yet
# It has ID_SERIAL_SHORT="BUR155132231"
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="A503Q18Q", SYMLINK+="riot/tty-pic32-wifire"
# sltb001a
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="1366", ATTRS{idProduct}=="1015", ATTRS{manufacturer}=="Silicon Labs", ATTRS{serial}=="000440068262", SYMLINK+="riot/tty-sltb001a"
# stm32f3discovery with uart2usb converter
# So the 'PORT' configuration is specific to the converter
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="CP2102 USB to UART Bridge Controller", ATTRS{serial}=="32f3", SYMLINK+="riot/tty-stm32f3discovery"


# Udev only parses SUBSYSTEM and one parent. For others, we will rely on ENV
# variables defined by 60-serial.rules
# So the current filename should be higher than 60-serial.rules
#
# Documentation:
#
# * The whole documentation from the following and also specifically this part
#   http://reactivated.net/writing_udev_rules.html#udevinfo
# * The variables definition in
#   /lib/udev/rules.d/60-serial.rules

# cc2650-launchpad
SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ENV{ID_VENDOR_ID}=="0451", ENV{ID_MODEL_ID}=="bef3", ENV{ID_SERIAL_SHORT}=="L1000973", ENV{ID_USB_INTERFACE_NUM}=="00", SYMLINK+="riot/tty-cc2650-launchpad"

# Debug rule, help finding about these board
# Remember to comment it after_again
# SYMLINK+="riot/debug/ttyCURRENT_$env{ID_SERIAL_SHORT}_$env{ID_VENDOR_ID}_$env{ID_MODEL_ID}_$env{ID_USB_INTERFACE_NUM}_%n"
